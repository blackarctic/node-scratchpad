# Node Scratchpad

A scratchpad for writing Node.js.

Has full support for:

- live reloading
- environment variables
- linting
- formatting
- testing
- dependency updating

## Getting Started

1. Install dependencies

```bash
nvm install && nvm use && yarn install
```

2. Copy `.env.template` to `.env` and update with your environment variable values.

3. (Optional) If using VSCode, copy `.vscode/settings.template.json` to `.vscode/settings.json`.

4. Start the app

```bash
yarn start
```

5. Write code. Start with `src/main.ts` & `src/helpers.ts`. (It should auto-reload on save.)
