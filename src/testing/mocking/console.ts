const mockConsoleFn = (fnName: "log" | "error" | "warn") => {
  const mockOfConsoleFn = jest.fn();
  const origConsoleFn = global.console[fnName];
  global.console[fnName] = mockOfConsoleFn;

  return {
    restoreConsoleFn: () => {
      global.console[fnName] = origConsoleFn;
    },
    mockOfConsoleFn,
  };
};

export const mockConsoleLog = () => {
  const result = mockConsoleFn("log");
  return {
    restoreConsoleLog: result.mockOfConsoleFn,
    mockOfConsoleLog: result.mockOfConsoleFn,
  };
};

export const mockConsoleError = () => {
  const result = mockConsoleFn("error");
  return {
    restoreConsoleError: result.mockOfConsoleFn,
    mockOfConsoleError: result.mockOfConsoleFn,
  };
};

export const mockConsoleWarn = () => {
  const result = mockConsoleFn("warn");
  return {
    restoreConsoleWarn: result.mockOfConsoleFn,
    mockOfConsoleWarn: result.mockOfConsoleFn,
  };
};
