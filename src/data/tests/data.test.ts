import { getData } from "../data";

describe("getData", () => {
  it("works as expected", async () => {
    const data = await getData();
    expect(data).toBeDefined();
  });
});
