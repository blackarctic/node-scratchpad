import data from "./data.json";

export const getData = async () => {
  // This function is async in anticipation
  // of more sophisticated data fetching.

  return data.data;
};
