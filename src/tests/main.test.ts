import { mockConsoleLog } from "../testing/mocking/console";

import { main } from "../main";

describe("main", () => {
  it("works as expected", async () => {
    const { mockOfConsoleLog, restoreConsoleLog } = mockConsoleLog();

    await main();

    expect(mockOfConsoleLog).toHaveBeenCalledTimes(2);
    expect(mockOfConsoleLog).toHaveBeenNthCalledWith(1, "Running...");
    expect(mockOfConsoleLog).toHaveBeenNthCalledWith(2, "Done");

    restoreConsoleLog();
  });
});
