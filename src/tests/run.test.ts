import { main } from "./run.test.mocks";

describe("run", () => {
  it("works as expected", () => {
    jest.requireActual("../run");
    expect(main).toHaveBeenCalledTimes(1);
    expect(main).toHaveBeenCalledWith();
  });
});
