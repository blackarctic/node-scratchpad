import once from "lodash/once";
import * as dotenv from "dotenv";

dotenv.config();

export const getEnv = once(() => {
  // This function is preferred over directly accessing process.env
  // because this layer allows us the ability to:
  // - run checks
  // - set defaults
  // - easily mock when testing

  // This is the only place where process.env should be accessed.
  // eslint-disable-next-line node/no-process-env
  return process.env;
});
