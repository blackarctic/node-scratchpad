import "./env.test.mocks";

const mockProcessEnv = () => {
  /* eslint-disable node/no-process-env */
  const mockOfProcessEnv: Record<string, string> = {
    MOCKED_PROCESS_ENV: "true",
  };
  const origProcessEnv = process.env;
  delete (process as any).env;
  (process as any).env = mockOfProcessEnv;

  return {
    restoreProcessEnv: () => {
      process.env = origProcessEnv;
    },
    mockOfProcessEnv,
  };
  /* eslint-enable node/no-process-env */
};

describe("env", () => {
  it("should initialize and provide the env", () => {
    const { restoreProcessEnv } = mockProcessEnv();

    const moduleExports = jest.requireActual("../env");

    const env = moduleExports.getEnv();
    expect(env["MOCKED_PROCESS_ENV"]).toBe("true");

    restoreProcessEnv();
    jest.resetModules();
  });
  it("should only execute the function contents of getEnv once", () => {
    const { restoreProcessEnv } = mockProcessEnv();

    const moduleExports = jest.requireActual("../env");

    expect(moduleExports.getEnv["CALLED_WITH_LODASH_ONCE"]).toBe(true);
    expect(moduleExports.getEnv["LODASH_ONCE_ARGS"]).toEqual([]);

    restoreProcessEnv();
    jest.resetModules();
  });
});
