export const once = jest.fn((fn, ...restOfArgs) => {
  fn.CALLED_WITH_LODASH_ONCE = true;
  fn.LODASH_ONCE_ARGS = restOfArgs;
  return fn;
});

jest.doMock("lodash/once", () => once);
