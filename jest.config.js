const jestConfig = {
  clearMocks: true,
  restoreMocks: true,
  resetMocks: false,
  watchPlugins: [
    "jest-watch-typeahead/filename",
    "jest-watch-typeahead/testname",
  ],
  collectCoverageFrom: [
    "src/**/*.{js,ts}",
    "!src/**/mock(s|ing)?/**/*.{js,ts}",
    "!src/**/test(s|ing)?/**/*.{js,ts}",
    "!node_modules/",
    "!dist",
  ],
  setupFilesAfterEnv: ["<rootDir>/config/jest/setupTests.js"],
  testMatch: ["<rootDir>/src/**/?(*.)(test).{js,ts}"],
  transform: {
    "^.+\\.ts$": "ts-jest",
  },
  transformIgnorePatterns: ["[/\\\\]node_modules[/\\\\].+\\.(js|ts)$"],
  moduleFileExtensions: ["js", "ts"],
  coverageDirectory: "coverage/jest",
  coverageThreshold: {
    global: {
      statements: 0,
      branches: 0,
      functions: 0,
      lines: 0,
    },
  },
};

module.exports = jestConfig;
