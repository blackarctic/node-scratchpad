const eslintConfig = {
  env: {
    es6: true,
    node: true,
    jest: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:jest/recommended",
  ],
  plugins: ["@typescript-eslint", "jest"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2022,
    sourceType: "module",
  },
  overrides: [
    {
      // Overrides for js files
      files: ["*.js"],
      extends: ["plugin:node/recommended-script"],
      rules: {
        // JS files use require statements since we only use them for tooling
        "@typescript-eslint/no-var-requires": "off",
        // JS files use process.env directly since we only use them for tooling
        // and they cannot (and should not) access the TS getEnv function.
        "node/no-process-env": "off",
      },
    },
  ],
  rules: {
    // Don't check unused vars with eslint. Typescript does a better job of this.
    "no-unused-vars": "off",
    // Use "type" over "interface".
    // Types can do all interfaces can and are more flexible.
    "@typescript-eslint/consistent-type-definitions": ["error", "type"],
    // Allow the use of explicit any. Sometimes, typing something just isn't worth it.
    "@typescript-eslint/no-explicit-any": ["off"],
    // process.env should only be accessed through the getEnv function.
    "node/no-process-env": "error",
    // No double equals.
    eqeqeq: ["error", "always"],
    // Types must be in PascalCase.
    "@typescript-eslint/naming-convention": [
      "error",
      {
        selector: ["interface", "typeAlias"],
        format: ["PascalCase"],
        custom: {
          regex: "^I[A-Z]",
          match: false,
        },
      },
    ],
  },
};

module.exports = eslintConfig;
