// Rules for major, minor, and patch updates

const minorRules = require("./minor/.ncurc");

const majorRules = {
  reject: [
    // Do not update node types as we are pinned in .nvmrc.
    "@types/node",
    // Do not update jest until ts-jest is compatible.
    "@types/jest",
    "jest",
  ],
};

module.exports = {
  reject: [...minorRules.reject, ...majorRules.reject],
};
