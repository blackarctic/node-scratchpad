const enableConsole = process.env.ENABLE_CONSOLE;
const allowWarnings = process.env.ALLOW_WARNINGS;

// Fail on unhandled promise rejection
process.on("unhandledRejection", (e) => {
  throw e;
});

const throwError = (s) => {
  if (s instanceof Error) {
    throw s;
  }
  throw new Error(s);
};

// Turn off console during tests, unless explicitly requested.
if (enableConsole) {
  global.console = {
    debug: global.console.debug,
    error: throwError,
    group: global.console.group,
    info: global.console.info,
    log: global.console.log,
    table: global.console.table,
    warn: allowWarnings ? global.console.warn : throwError,
  };
} else {
  global.console = {
    debug: jest.fn(),
    error: throwError,
    group: jest.fn(),
    info: jest.fn(),
    log: jest.fn(),
    table: jest.fn(),
    warn: allowWarnings ? jest.fn() : throwError,
  };
}
